using Autofac.Core;
using Autofac;
using System.Windows.Forms.Design;
using BGD_Services.Services;
using BGD_Services.IServices;
using WinFormsApp.AutofacInject;

namespace WinFormsApp
{
    static class Program
    {
        private static IContainer _container;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {            
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            var builder = new ContainerBuilder();

            // ����Autofac����
            builder.RegisterModule(new AutofacModuleRegister());
            builder.RegisterType<MainForm>().AsSelf();

            _container = builder.Build();

            Application.Run(_container.Resolve<MainForm>());

            ApplicationConfiguration.Initialize();
        }
    }
}