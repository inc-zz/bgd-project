using BGD_Repository.Enums;
using BGD_Repository.SqlSugarOrm;
using BGD_Services.IServices;
using BGD_Services.Services;
using Microsoft.Extensions.DependencyInjection;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace WinFormsApp
{
    public partial class MainForm : Form
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IDeclarationServices _declarationServices;

        public MainForm()
        {
            InitializeComponent();

            // 配置服务容器（如果你使用DI）
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            _serviceProvider = serviceCollection.BuildServiceProvider();
            _declarationServices = _serviceProvider.GetService<IDeclarationServices>();

            // 调用方法并显示结果
            LoadDeclarationsAsync();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SqlSugarDbContextOptions>(options =>
            {
                options.Connection = "Server=.;Database=customsdeclaration;User Id=sa;Password=MsSql@123456;;Encrypt=True;TrustServerCertificate=True;"; // 替换为你的数据库连接字符串
                options.DbType = DbTypeNameEnum.MsSql; // 根据你的数据库类型设置

            });
            services.AddScoped<IDeclarationServices, DeclarationServices>();
            services.AddSingleton<SqlSugarDbContextOptions>();
        }

        private async void LoadDeclarationsAsync()
        {
            try
            {
                var declarations = await _declarationServices.GetDeclarations();
                // 处理获取到的数据，比如绑定到DataGridView等
                dataGridView1.DataSource = declarations;
                MessageBox.Show($"获取到 {declarations.Count} 条数据。");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"获取数据失败: {ex.Message}");
            }
        }

        /// <summary>
        /// 读取远程数据，加载到列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadDataBtn_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 写入数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertDbBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
