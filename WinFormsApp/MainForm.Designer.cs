﻿namespace WinFormsApp
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dataGridView1 = new DataGridView();
            EnterpriseNO = new DataGridViewTextBoxColumn();
            DeclarationType = new DataGridViewTextBoxColumn();
            DeclarationLocation = new DataGridViewTextBoxColumn();
            ContactPhone = new DataGridViewTextBoxColumn();
            CustomsContact = new DataGridViewTextBoxColumn();
            InsideConsigneeCode = new DataGridViewTextBoxColumn();
            InsideCreditCodeInside = new DataGridViewTextBoxColumn();
            LoadDataBtn = new Button();
            InsertDbBtn = new Button();
            Import = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { EnterpriseNO, DeclarationType, DeclarationLocation, ContactPhone, CustomsContact, InsideConsigneeCode, InsideCreditCodeInside });
            dataGridView1.Location = new Point(12, 79);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.Size = new Size(1080, 150);
            dataGridView1.TabIndex = 0;
            dataGridView1.CellContentClick += dataGridView1_CellContentClick;
            // 
            // EnterpriseNO
            // 
            EnterpriseNO.HeaderText = "企业内部编号";
            EnterpriseNO.Name = "EnterpriseNO";
            // 
            // DeclarationType
            // 
            DeclarationType.HeaderText = "报关类型";
            DeclarationType.Name = "DeclarationType";
            // 
            // DeclarationLocation
            // 
            DeclarationLocation.HeaderText = "申报地";
            DeclarationLocation.Name = "DeclarationLocation";
            // 
            // ContactPhone
            // 
            ContactPhone.HeaderText = "联系人手机";
            ContactPhone.Name = "ContactPhone";
            // 
            // CustomsContact
            // 
            CustomsContact.HeaderText = "报关联系人";
            CustomsContact.Name = "CustomsContact";
            // 
            // InsideConsigneeCode
            // 
            InsideConsigneeCode.HeaderText = "境内收发货人代码";
            InsideConsigneeCode.Name = "InsideConsigneeCode";
            // 
            // InsideCreditCodeInside
            // 
            InsideCreditCodeInside.FillWeight = 260F;
            InsideCreditCodeInside.HeaderText = "境内收发货人18位社会信用代码";
            InsideCreditCodeInside.Name = "InsideCreditCodeInside";
            InsideCreditCodeInside.Width = 260;
            // 
            // LoadDataBtn
            // 
            LoadDataBtn.Location = new Point(21, 36);
            LoadDataBtn.Name = "LoadDataBtn";
            LoadDataBtn.Size = new Size(106, 23);
            LoadDataBtn.TabIndex = 1;
            LoadDataBtn.Text = "加载远程数据";
            LoadDataBtn.UseVisualStyleBackColor = true;
            LoadDataBtn.Click += LoadDataBtn_Click;
            // 
            // InsertDbBtn
            // 
            InsertDbBtn.Location = new Point(147, 36);
            InsertDbBtn.Name = "InsertDbBtn";
            InsertDbBtn.Size = new Size(111, 23);
            InsertDbBtn.TabIndex = 2;
            InsertDbBtn.Text = "写入本地数据库";
            InsertDbBtn.UseVisualStyleBackColor = true;
            InsertDbBtn.Click += InsertDbBtn_Click;
            // 
            // Import
            // 
            Import.Location = new Point(275, 36);
            Import.Name = "Import";
            Import.Size = new Size(75, 23);
            Import.TabIndex = 3;
            Import.Text = "导入本地Excel";
            Import.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1104, 565);
            Controls.Add(Import);
            Controls.Add(InsertDbBtn);
            Controls.Add(LoadDataBtn);
            Controls.Add(dataGridView1);
            Name = "MainForm";
            Text = "Form1";
            Load += Form1_Load;
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn EnterpriseNO;
        private DataGridViewTextBoxColumn DeclarationType;
        private DataGridViewTextBoxColumn DeclarationLocation;
        private DataGridViewTextBoxColumn ContactPhone;
        private DataGridViewTextBoxColumn CustomsContact;
        private DataGridViewTextBoxColumn InsideConsigneeCode;
        private DataGridViewTextBoxColumn InsideCreditCodeInside;
        private Button LoadDataBtn;
        private Button InsertDbBtn;
        private Button Import;
    }
}
