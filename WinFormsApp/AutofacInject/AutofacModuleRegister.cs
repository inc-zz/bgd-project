﻿using Autofac;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp.AutofacInject
{

    /// <summary>
    /// 
    /// </summary>
    public class AutofacModuleRegister : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            string codeBase = currentAssembly.CodeBase;
            Uri uri = new Uri(codeBase);
            string localPath = Uri.UnescapeDataString(uri.LocalPath);

            // 获取程序集所在的目录
            string directory = Path.GetDirectoryName(localPath);

            builder.RegisterType<MainForm>().PropertiesAutowired();
            var assemblyRepository = Assembly.LoadFrom($"{directory}\\BGD_Repository.dll");
            builder.RegisterAssemblyTypes(assemblyRepository).InstancePerDependency().AsImplementedInterfaces();
            var assemblyService = Assembly.LoadFrom($"{directory}\\BGD_Services.dll");
            builder.RegisterAssemblyTypes(assemblyService).InstancePerDependency().AsImplementedInterfaces();

        }
    }
}
