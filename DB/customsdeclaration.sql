/*
 Navicat Premium Data Transfer

 Source Server         : local_docker_sqlserver2019
 Source Server Type    : SQL Server
 Source Server Version : 15004410
 Source Host           : localhost:1433
 Source Catalog        : customsdeclaration
 Source Schema         : bgd

 Target Server Type    : SQL Server
 Target Server Version : 15004410
 File Encoding         : 65001

 Date: 25/11/2024 00:47:08
*/


-- ----------------------------
-- Table structure for tb_declarations
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[bgd].[tb_declarations]') AND type IN ('U'))
	DROP TABLE [bgd].[tb_declarations]
GO

CREATE TABLE [bgd].[tb_declarations] (
  [ID] int  NOT NULL,
  [EnterpriseNO] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [DeclarationType] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [DeclarationLocation] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [CustomsContact] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ContactPhone] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsideConsigneeCode] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsideCreditCodeInside] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsideConsigneeCompanyName] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [CompanyCreditCode] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [CompanyName] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ExternalOverseasConsignor] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ForeignShipperName] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsideConsigneePhone] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsideConsigneeAddress] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ExternalConsigneePhone] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ExternalConsigneeAddress] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ContractSigningLocation] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ContractSigningDate] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InvoiceNumber] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [PackagingType] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [AgreementNumber] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [SupervisionMethod] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [DutyNature] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [FinalDestinationCountry] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [TradingCountry] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [FreightCost] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [FreightCurrency] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [FreightType] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [RecordNumber] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [ResponsibleCustoms] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsuranceCost] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsuranceCurrency] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [InsuranceType] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [LicenseNumber] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [TransactionCurrency] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [MiscellaneousFees] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [MiscellaneousFeesCurrency] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [MiscellaneousFeesType] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [TaxRefundable] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [DutyStatus] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [DestinationPort] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [CountryOfArrival] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [TransportVehicleName] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [WaybillNumber] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [SpecialRelationship] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [PriceImpact] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [RoyaltyPayment] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [Remarks] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [EntrustmentType] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [EntrustmentPerBill] nvarchar(255) COLLATE Latin1_General_CI_AS  NULL
)
GO

ALTER TABLE [bgd].[tb_declarations] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'自增ID',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ID'
GO

EXEC sp_addextendedproperty
'MS_Description', N'企业内部编号',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'EnterpriseNO'
GO

EXEC sp_addextendedproperty
'MS_Description', N'报关类型',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'DeclarationType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'申报地',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'DeclarationLocation'
GO

EXEC sp_addextendedproperty
'MS_Description', N'报关联系人',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'CustomsContact'
GO

EXEC sp_addextendedproperty
'MS_Description', N'联系人手机',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ContactPhone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境内收发货人代码',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsideConsigneeCode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境内收发货人18位社会信用代码',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsideCreditCodeInside'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境内收发货人企业海关名称',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsideConsigneeCompanyName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'生产销售单位18位社会信用代码',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'CompanyCreditCode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'生产销售单位企业海关名称',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'CompanyName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境外收发货人代码',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ExternalOverseasConsignor'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境外收发货人中文名称 ',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ForeignShipperName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境内收发货人电话',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsideConsigneePhone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境内收发货人地址',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsideConsigneeAddress'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境外收发货人电话',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ExternalConsigneePhone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'境外收发货人地址',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ExternalConsigneeAddress'
GO

EXEC sp_addextendedproperty
'MS_Description', N'签约地点',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ContractSigningLocation'
GO

EXEC sp_addextendedproperty
'MS_Description', N'签约日期',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ContractSigningDate'
GO

EXEC sp_addextendedproperty
'MS_Description', N'发票号',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InvoiceNumber'
GO

EXEC sp_addextendedproperty
'MS_Description', N'包装种类',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'PackagingType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'合同协议号',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'AgreementNumber'
GO

EXEC sp_addextendedproperty
'MS_Description', N'监管方式',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'SupervisionMethod'
GO

EXEC sp_addextendedproperty
'MS_Description', N'征免性质',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'DutyNature'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最终目的国',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'FinalDestinationCountry'
GO

EXEC sp_addextendedproperty
'MS_Description', N'贸易国',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'TradingCountry'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运费',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'FreightCost'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运费币种',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'FreightCurrency'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运费类型',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'FreightType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备案号',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'RecordNumber'
GO

EXEC sp_addextendedproperty
'MS_Description', N'主管海关',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'ResponsibleCustoms'
GO

EXEC sp_addextendedproperty
'MS_Description', N'保费',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsuranceCost'
GO

EXEC sp_addextendedproperty
'MS_Description', N'保费币种',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsuranceCurrency'
GO

EXEC sp_addextendedproperty
'MS_Description', N'保费类型',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'InsuranceType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'许可证号',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'LicenseNumber'
GO

EXEC sp_addextendedproperty
'MS_Description', N'成交币制',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'TransactionCurrency'
GO

EXEC sp_addextendedproperty
'MS_Description', N'杂费',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'MiscellaneousFees'
GO

EXEC sp_addextendedproperty
'MS_Description', N'杂费币种',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'MiscellaneousFeesCurrency'
GO

EXEC sp_addextendedproperty
'MS_Description', N'杂费类型 ',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'MiscellaneousFeesType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否退税',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'TaxRefundable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'征免',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'DutyStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'指运港',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'DestinationPort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运抵国',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'CountryOfArrival'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运输工具名称',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'TransportVehicleName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'提运单号 航次号',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'WaybillNumber'
GO

EXEC sp_addextendedproperty
'MS_Description', N'特殊关系确认',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'SpecialRelationship'
GO

EXEC sp_addextendedproperty
'MS_Description', N'价格影响确认',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'PriceImpact'
GO

EXEC sp_addextendedproperty
'MS_Description', N'支付特许权使用费确认',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'RoyaltyPayment'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注 ',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'Remarks'
GO

EXEC sp_addextendedproperty
'MS_Description', N'委托类型',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'EntrustmentType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'逐票电子委托',
'SCHEMA', N'bgd',
'TABLE', N'tb_declarations',
'COLUMN', N'EntrustmentPerBill'
GO


-- ----------------------------
-- Records of tb_declarations
-- ----------------------------
BEGIN TRANSACTION
GO

INSERT INTO [bgd].[tb_declarations] VALUES (N'1', N'111', N'111', N'22', N'2', N'13213', N'21321', N'32121', N'32132', N'1321', N'12312', N'3213', N'3543', N'123123', N'1', N'2', N'4', N'5', N'4', N'2', N'3', N'6', N'7', N'8', N'9', N'21', N'6', N'2', N'5', N'6', N'4', N'2', N'5', N'1', N'2', N'4', N'773213', N'8佛挡杀佛', N'44房贷似', N'33???', N'3', N'3发生的', N'sfds', N'5', N'8', N'9', N'房贷似', N'3', N'6', N'63', N'2'), (N'2', N'222', N'111', N'22', N'2', N'13213', N'21321', N'32121', N'32132', N'1321', N'213', N'343', N'43', N'321321', N'1', N'2', N'65', N'4', N'5', N'2', N'4', N'5', NULL, N'7', N'2', N'4', N'4', N'3', N'4', N'8', N'7', N'4', N'4', N'2', N'1', N'5', N'6', N'99萨达打', N'33', N'222', N'2', N'2', N'sszgdfs反攻倒算', N'6', N'7', N'0', N'给梵蒂冈', N'5', N'7', N'44', N'2')
GO

COMMIT
GO


-- ----------------------------
-- Primary Key structure for table tb_declarations
-- ----------------------------
ALTER TABLE [bgd].[tb_declarations] ADD CONSTRAINT [PK__tb_decla__3214EC27D2F5EBCC] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

