﻿using BGD_Repository.Enums;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGD_Repository.SqlSugarOrm;

public class SqlSugarDbContextOptions:IOptions<SqlSugarDbContextOptions>
{

    /// <summary>
    /// 连接字符串
    /// </summary>
    public string? Connection { get; set; }

    public DbTypeNameEnum DbType { get; set; }

    public SqlSugarDbContextOptions Value { get { return this; } }

}
