﻿using BGD_Repository.Entities;
using BGD_Repository.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SqlSugar;
using System.Configuration;
using System.Data;

namespace BGD_Repository.SqlSugarOrm;

/// <summary>
/// 数据库上下文
/// </summary>
public abstract class SqlSugarDbContext
{

    /// <summary>
    /// 用来处理事务多表查询和复杂的操作
    /// </summary>
    public SqlSugarClient DbContext;
    /// <summary>
    /// 配置信息
    /// </summary>
    private readonly SqlSugarDbContextOptions _options;

    protected abstract IDbConnection CreateConnection(string connectionString, DbTypeNameEnum dbType);

    public SqlSugarDbContext(IOptions<SqlSugarDbContextOptions> optionsAccessor) 
    { 
        _options = optionsAccessor.Value;

        var _connection = CreateConnection(_options.Connection, _options.DbType);

        DbContext = new SqlSugarClient(new ConnectionConfig()
        {
            DbType = SqlSugar.DbType.SqlServer,
            InitKeyType = InitKeyType.Attribute,
            IsAutoCloseConnection = true,
            ConnectionString = _connection.ConnectionString
        });
    }

    #region 数据表1

    public SimpleClient<Declarations> DeclarationsDb => new SimpleClient<Declarations>(DbContext);//报关单表

    #endregion

}
