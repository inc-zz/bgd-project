﻿using BGD_Repository.Enums;
using Microsoft.Extensions.Options;
using MySqlConnector;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGD_Repository.SqlSugarOrm;

/// <summary>
/// 
/// </summary>
public class ProjectSqlSugarContext : SqlSugarDbContext
{

    public ProjectSqlSugarContext(IOptions<SqlSugarDbContextOptions> optionsAccessor) : base(optionsAccessor)
    {
    }

    protected override IDbConnection CreateConnection(string connectionString, DbTypeNameEnum dbType = DbTypeNameEnum.MsSql)
    {
        IDbConnection conn = new SqlConnection(connectionString);
        return conn;
    }
}
