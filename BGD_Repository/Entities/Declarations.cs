﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGD_Repository.Entities
{
    /// <summary>
    /// 报关数据表
    /// </summary>

    [SugarTable("bgd.tb_declarations")]
    public class Declarations
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true)]
        public string ID { get; set; }
        public string EnterpriseNO { get; set; }
        public string DeclarationType { get; set; }
        public string DeclarationLocation { get; set; }
        public string CustomsContact { get; set; }
        public string ContactPhone { get; set; }
        public string InsideConsigneeCode { get; set; }
        public string InsideCreditCodeInside { get; set; }
        public string InsideConsigneeCompanyName { get; set; }
        public string CompanyCreditCode { get; set; }
        public string CompanyName { get; set; }
        public string ExternalOverseasConsignor { get; set; }
        public string ForeignShipperName { get; set; }
        public string InsideConsigneePhone { get; set; }
        public string InsideConsigneeAddress { get; set; }
        public string ExternalConsigneePhone { get; set; }
        public string ExternalConsigneeAddress { get; set; }
        public string ContractSigningLocation { get; set; }
        public string ContractSigningDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string PackagingType { get; set; }
        public string AgreementNumber { get; set; }
        public string SupervisionMethod { get; set; }
        public string DutyNature { get; set; }
        public string FinalDestinationCountry { get; set; }
        public string TradingCountry { get; set; }
        public string FreightCost { get; set; }
        public string FreightCurrency { get; set; }
        public string FreightType { get; set; }
        public string RecordNumber { get; set; }
        public string ResponsibleCustoms { get; set; }
        public string InsuranceCost { get; set; }
        public string InsuranceCurrency { get; set; }
        public string InsuranceType { get; set; }
        public string LicenseNumber { get; set; }
        public string TransactionCurrency { get; set; }
        public string MiscellaneousFees { get; set; }
        public string MiscellaneousFeesCurrency { get; set; }
        public string MiscellaneousFeesType { get; set; }
        public string TaxRefundable { get; set; }
        public string DutyStatus { get; set; }
        public string DestinationPort { get; set; }
        public string CountryOfArrival { get; set; }
        public string TransportVehicleName { get; set; }
        public string WaybillNumber { get; set; }
        public string SpecialRelationship { get; set; }
        public string PriceImpact { get; set; }
        public string RoyaltyPayment { get; set; }
        public string Remarks { get; set; }
        public string EntrustmentType { get; set; }
        public string EntrustmentPerBill { get; set; }


    }
}
