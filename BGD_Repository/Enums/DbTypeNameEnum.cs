﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGD_Repository.Enums;

public enum DbTypeNameEnum
{

    MySQL = 0,

    MsSql = 1,

    PgSql = 2,

    Oracle = 3


}
