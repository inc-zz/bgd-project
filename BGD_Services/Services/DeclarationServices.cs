﻿using BGD_Repository.Entities;
using BGD_Repository.SqlSugarOrm;
using BGD_Services.IServices;
using Microsoft.Extensions.Options;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGD_Services.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class DeclarationServices : ProjectSqlSugarContext, IDeclarationServices
    {
        /// <summary>
        /// 
        /// </summary>
        public DeclarationServices(IOptions<SqlSugarDbContextOptions> optionsAccessor) : base(optionsAccessor)
        {

        }

        public ISqlSugarClient Context { get; set; }

        /// <summary>
        /// 从A库读取数据集合
        /// </summary>
        /// <returns></returns>
        public async Task<List<Declarations>> GetDeclarations()
        {
            try
            {
                //文档：https://www.donet5.com/Home/Doc?typeId=1218
                //两种模式都可以
                var list1 = await DbContext.Queryable<Declarations>().ToListAsync();
                var list = await DeclarationsDb.GetListAsync();

                return list;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

    }
}
