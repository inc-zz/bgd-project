﻿using BGD_Repository.Entities;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGD_Services.IServices
{
    public interface IDeclarationServices : ISugarRepository
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<Declarations>> GetDeclarations();
    }
}
